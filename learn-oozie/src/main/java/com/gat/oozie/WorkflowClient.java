package com.gat.oozie;

import java.io.InputStream;
import java.util.Properties;

import org.apache.oozie.client.OozieClient;
import org.apache.oozie.client.WorkflowJob;
import org.apache.oozie.client.WorkflowJob.Status;

import com.gat.utils.HDFSUtils;

public class WorkflowClient {
	private static String OOZIE_URL = "http://10.101.9.198:11000/oozie";

	public static void main(String[] args) throws Exception {
		// 上传最新配置文件到HDFS文件系统
		String path = WorkflowClient.class.getResource("/job.properties").getPath();
		String realPath = path.substring(1);
		System.out.println(path);
		HDFSUtils.putFile(realPath, "/user/wgq/regular-email/job.properties");
		// 启动Oozie工作流
		OozieClient client = new OozieClient(OOZIE_URL);
		Properties conf = client.createConfiguration();
		InputStream is = WorkflowClient.class.getClassLoader().getResourceAsStream("job.properties");
		conf.load(is);
		String jobId = client.run(conf);
		System.out.println("jobId=" + jobId);
		WorkflowJob job = client.getJobInfo(jobId);
		Status status = job.getStatus();
		if (status == Status.RUNNING) {
			System.out.println("Workflow job running");
		} else {
			System.out.println("Problem starting Workflow job");
		}
	}

}
