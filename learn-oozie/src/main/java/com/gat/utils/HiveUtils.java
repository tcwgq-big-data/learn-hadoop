package com.gat.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class HiveUtils {
	public static void main(String[] args) {
		String driverName = "org.apache.hive.jdbc.HiveDriver";
		String url = "jdbc:hive2://10.101.9.197:10000/test";
		String user = "wgq";
		String password = "112113";
		String sql = "select * from webservicelog limit 100000";
		try {
			Class.forName(driverName);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		try {
			Connection conn = DriverManager.getConnection(url, user, password);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				System.out.println(rs.getString(1) + "--" + rs.getString(2));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
}
